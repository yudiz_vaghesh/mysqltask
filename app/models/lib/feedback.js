const { Sequelize } = require('sequelize');
const sequelize = require('../index');
// const  { User }  = require('models');
// const   User   = require('models');
const User = require('../../models/lib/users');

const Feedback = sequelize.define('feedback',{
    fID:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uID:{
        type: Sequelize.INTEGER,
        references: {
           model: 'user', // 'fathers' refers to table name
           key: 'uID', // 'id' refers to column name in fathers table
        }
    },
    message:{
        type: Sequelize.TEXT
    }
}, // attribute 
{
    freezeTableName: true, // the table name to be equal to the model name, without any modifications: 
    indexes: [
        // add a FULLTEXT index
        { type: 'FULLTEXT', name: 'msg1', fields: ['message'] }
      ]
}
);

// one to many relationship
User.hasMany(Feedback);
Feedback.belongsTo(User);

Feedback.sync();

module.exports = Feedback;