const { Sequelize } = require('sequelize');
const sequelize = require('../index');
    
    const User = sequelize.define('user', {
         
        uID :{
            type: Sequelize.INTEGER, // why we not able to use UUID
            primaryKey: true,
            autoIncrement: true
        },
        uName: { 
            type: Sequelize.STRING, 
        }, 
        uQualification: { 
            type: Sequelize.STRING, 
        }, 
        uEmail: { 
            type: Sequelize.STRING, 
        }, 
        uPhone: { 
            type: Sequelize.BIGINT, 
        }, 
        }, // attribute 
        { 
            freezeTableName: true, // the table name to be equal to the model name, without any modifications: 
            tableName: 'user', // explicitly set table name 
        } // option 
    );
    
User.sync();
module.exports = User;
// how to remove this warnings