const dbConfig = require('../config/dbConfig');
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, { 
    host: dbConfig.HOST, 
    port: dbConfig.PORT,
    dialect: dbConfig.dialect, 
    operatorsAliases: false,
    pool: { 
        max: dbConfig.pool.max, 
        min: dbConfig.pool.min, 
        acquire: dbConfig.pool.acquire, 
        idle: dbConfig.pool.idle, 
    },     
}); 

try {
    sequelize.authenticate(); 
    console.log('Connection has been established successfully.'); 
    } catch (error) { 
    console.error('Unable to connect to the database:', error);
    } 

module.exports = sequelize;