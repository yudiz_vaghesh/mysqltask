const User = require('./lib/users');
const Feedback = require('./lib/feedback');

module.exports = { User, Feedback};