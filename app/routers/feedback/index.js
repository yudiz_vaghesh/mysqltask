const router = require('express').Router();
const controllers = require('./lib/controller');


// router.get('/',controllers.test);
router.post('/send/:id',controllers.sendFeedback);
router.post('/find',controllers.findFeedback);
router.get('/all',controllers.allUsers);

module.exports = router;