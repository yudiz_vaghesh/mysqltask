// const test = (req,res)=>{
//     res.send('Feedback API');
// };
const { Sequelize } = require('sequelize');
const { User, Feedback} = require('models');

const sendFeedback = async (req,res) => {
    try{
        let user = await User.findOne({where:{uID:req.params.id}});
        if(!user) return res.status(404).send('User not found...');

        let feedback = await Feedback.create({
            fID: req.body.id,
            uID: user.uID,
            message: req.body.msg
        });

        res.send(feedback);

    } catch(error){
        console.error('Not able to give feedback',error);
    }
};

const findFeedback = async (req,res) =>{
    try{
       
        let msg = await Feedback.findAll({
            where: Sequelize.literal('MATCH (message) AGAINST (:feedback)'),
            replacements: {
            feedback: req.body.text
            },
            attributes: ['uID', 'message']
            // offset:1,
            // limit: 1
        }); 

        if(!msg) return res.status(404).send('Not found...');
        res.send(msg);
    } catch(error){
        console.error('Not able to find feedback',error);
    }
};

const allUsers = async (req,res) =>{
    try{
        
        let users = await User.findAll({
            offset:3,
            limit: 3
        }); 

        if(!users) return res.status(404).send('Not found...');
        res.send(users);

    } catch(error){
        console.error('Error while limit the data.',error);
    }
};
module.exports = { sendFeedback, findFeedback,allUsers }