const router = require('express').Router();
const controllers = require('./lib/controller');


// router.post('/isEmailReceived', controllers.isEmailReceived); // This is for an example
router.get('/',controllers.test);
router.post('/insert',controllers.dataInsert);
router.put('/update/:id',controllers.dataUpdate);
router.delete('/delete/:id',controllers.dataDelete);

module.exports = router;
