const sequelize = require('../../../models');
const { User } = require('models');

const test = (req,res)=>{
    res.send('Hello');
};

const dataInsert = async (req,res)=>{
    try{
        console.log(req.body);
        let user = await User.create({
            uID: req.body.id,
            uName: req.body.name,
            uQualification: req.body.qualify,
            uEmail: req.body.email,
            uPhone: req.body.phone
        });
        res.send(user);
    } catch(error){
        console.error('Error while inserting data.',error);
    }
};

const dataUpdate = async (req,res) => {
    try{
        let { fname,qual,email } = req.body;
        console.log(req.params.id);
        let query = `UPDATE user SET ?  WHERE uID =${req.params.id}`;  //literal
        let data = {uName:fname,uQualification:qual,uEmail:email};
        let user = await sequelize.query(query,data);

        res.send(user);
    } catch(error){
        console.error('Error while updating a user details.',error);
    }
}
const dataDelete = async (req,res)=>{
    try{
        let user =  await User.destroy({where:{uID:req.params.id}});
        if(!user) return res.status(404).send('Something went wrong !!!');
        res.send(` User ${req.params.id} deleted !!! `);
    } catch(error){
        console.error('Error while updating a user details.',error);
    }
}

module.exports = { test, dataInsert, dataUpdate, dataDelete };