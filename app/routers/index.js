const router = require('express').Router();
const crudRoute = require('./crud');
const transRoute = require('./transaction');
const feedbackRoute = require('./feedback');

router.use('/crud', crudRoute);
router.use('/transaction',transRoute);
router.use('/feedback',feedbackRoute);

module.exports = router;
