const router = require('express').Router();
const controllers = require('./lib/controller');


// router.post('/isEmailReceived', controllers.isEmailReceived); // This is for an example

router.get('/',controllers.test);
router.post('/insert',controllers.transInsert);
router.put('/update/:id',controllers.transUpdate);
module.exports = router;