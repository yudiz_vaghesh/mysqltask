const sequelize = require("../../../models");
const {User} = require('models');

const test = (req,res)=>{
    res.send('Transaction test');
};

// transactions in mySql using Node
const transInsert = async (req,res) =>{
    let trans = await sequelize.transaction();
    try{
       
            let user = await User.create({
                    uID: req.body.id,
                    uName: req.body.name,
                    uQualification: req.body.qualify,
                    uEmail: req.body.email,
                    uPhone: req.body.phone
            },
            { transaction: trans});
            
            await trans.commit();
            res.send(user);
    } catch(error){
        trans.rollback();
        console.error('Error in transaction.',error);
    }
};

// lock is keep those rows locked in transaction untill transaction not commits
const transUpdate= async (req,res) =>{
    let trans = await sequelize.transaction();
    try{
       
        let { fname,qual,email } = req.body;
        console.log(req.params.id);
        // let query = `UPDATE user SET ?  WHERE uID =${req.params.id}`;
        let data = {uName:fname,uQualification:qual,uEmail:email};
        let user = await User.update(data,{where:{uID:req.params.id},transaction:trans,lock:true});

        await trans.commit();
        res.send('Updated Successfully !!');
    } catch(error){
        trans.rollback();
        console.error('Error in transaction.',error);
    }
};
module.exports = { test, transInsert, transUpdate };