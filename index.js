const express = require('express');
const bodyParser = require('body-parser');
const sql = require('./app/routers/index');

// initialize app
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extendedL:false}));

// routes
app.use('/sql',sql);
// connect.sequelize.sync({force:true});
require('./app/models/index');
//run localhost server
app.listen(8081,()=>{
    console.log('Localhost connected on 5000...');
});
